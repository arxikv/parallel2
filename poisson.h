#ifndef POISSON_H
#define POISSON_H

typedef double real;
#define MPI_R MPI_DOUBLE

#include <mpi.h>

#ifdef _OPENMP
#include <omp.h>
#endif

struct mpi_pair
{
    real beta;
    real gamma;
};

class Poisson
{
    // MPI
    int mpi_size;
    int mpi_rank;
    int mpi_size_x;  // >= mpi_size_y
    int mpi_size_y;
    int mpi_rank_x;
    int mpi_rank_y;
    int top;  // -1 if not exists
    int right;
    int bottom;
    int left;
    int reqs_count;
    MPI_Request *reqs_send;
    MPI_Request *reqs_recv;
    MPI_Datatype mpi_pair_type;
    MPI_Op merge_op;

    // Grid
    int grid_size;
    real grid_step;
    real sqr_grid_step;
    int brd_size_i;  // y
    int brd_size_j;  // x
    int brd_start_i;
    int brd_start_j;
    int ext_size_i;
    int ext_size_j;

    // Data
    real *F;
    real *pk;
    real *pkp1;
    real *r;
    real *g;
    real *dp;
    real *dr;
    real *dg;

    // Buffers
    real *send_t;
    real *send_r;
    real *send_b;
    real *send_l;
    real *recv_t;
    real *recv_r;
    real *recv_b;
    real *recv_l;

    real dgg;
    real alpha_local;
    real alpha_global;
    mpi_pair beta_gamma;
    real tau;

    void init_mpi_size_xy();
    void init_neighbors();
    void init_brd_size_ij();
    void init_ext(real *f);
    void init_mem();
    void init_mpi_pair();
    void init_F();
    void init_p();

    void laplacian(real *dst, const real *src, int i, int j);  // neg
    void laplacian_brd(real *dst, const real *src);
    void laplacian_innbrd(real *dst, const real *src, bool brd);

    void update_r_ext();
    void update_r_innbrd();
    void update_g();
    void update_p();

    real dot_brd(const real *a, const real *b);
    real dot_innbrd(const real *a, const real *b, bool brd);

    double eps;
    real criterion();
    void merge_alpha();
    void merge_beta_gamma();

    void to_buffers();
    void from_buffers();
    void exchange();
    void wait();

public:

    Poisson(int mpi_size_, int mpi_rank_, int grid_size_, double eps_);

    void init();

    void loop();

    void save(const char *path);

    void destroy();
};

#endif

