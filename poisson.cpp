// Архипенко Константин Владимирович, группа 628 ВМК МГУ

#include <cstring>
#include <cmath>
#include <fstream>
#include <iostream>

#include <sys/time.h>

#include "poisson.h"

Poisson::Poisson(int mpi_size_, int mpi_rank_, int grid_size_, double eps_)
{
    mpi_size = mpi_size_;
    mpi_rank = mpi_rank_;
    grid_size = grid_size_;
    grid_step = 2.0 / grid_size;
    sqr_grid_step = grid_step * grid_step;
    eps = eps_;
}

void Poisson::init_mpi_size_xy()
{
    int tmp = mpi_size;
    bool odd = false;
    mpi_size_x = 1;
    mpi_size_y = mpi_size;

    while (tmp >>= 1) {
        if (odd = !odd) {
            mpi_size_x <<= 1;
            mpi_size_y >>= 1;
        }
    }

    mpi_rank_x = mpi_rank % mpi_size_x;
    mpi_rank_y = mpi_rank / mpi_size_x;
}

void Poisson::init_neighbors()
{
    top = mpi_rank_y == 0 ? -1 : mpi_rank - mpi_size_x;
    right = mpi_rank_x == mpi_size_x - 1 ? -1 : mpi_rank + 1;
    bottom = mpi_rank_y == mpi_size_y - 1 ? -1 : mpi_rank + mpi_size_x;
    left = mpi_rank_x == 0 ? -1 : mpi_rank - 1;
}

void Poisson::init_brd_size_ij()
{
    int reg_size_i = (grid_size - 1) / mpi_size_y;
    int reg_size_j = (grid_size - 1) / mpi_size_x;
    int irreg_count_i = (grid_size - 1) % mpi_size_y;
    int irreg_count_j = (grid_size - 1) % mpi_size_x;

    if (mpi_rank_y < irreg_count_i) {
        brd_size_i = reg_size_i + 1;
        brd_start_i = mpi_rank_y * brd_size_i + 1;
    } else {
        brd_size_i = reg_size_i;
        brd_start_i = irreg_count_i * (reg_size_i + 1) + 1;
        brd_start_i += (mpi_rank_y - irreg_count_i) * brd_size_i;
    }

    if (mpi_rank_x < irreg_count_j) {
        brd_size_j = reg_size_j + 1;
        brd_start_j = mpi_rank_x * brd_size_j + 1;
    } else {
        brd_size_j = reg_size_j;
        brd_start_j = irreg_count_j * (reg_size_j + 1) + 1;
        brd_start_j += (mpi_rank_x - irreg_count_j) * brd_size_j;
    }

    ext_size_i = brd_size_i + 2;
    ext_size_j = brd_size_j + 2;
}

void Poisson::init_ext(real *f)
{
    #pragma omp parallel
    {
        if (top < 0) {
            #pragma omp for
            for (int j = 0; j < ext_size_j; ++j)
                f[j] = 0.0;
        }

        if (right < 0) {
            #pragma omp for
            for (int i = 0; i < ext_size_i; ++i)
                f[(i + 1) * ext_size_j - 1] = 0.0;
        }

        if (bottom < 0) {
            #pragma omp for
            for (int j = 0; j < ext_size_j; ++j)
                f[(ext_size_i - 1) * ext_size_j + j] = 0.0;
        }

        if (left < 0) {
            #pragma omp for
            for (int i = 0; i < ext_size_i; ++i)
                f[i * ext_size_j] = 0.0;
        }
    }
}

void Poisson::init_mem()
{
    F = new real[ext_size_i * ext_size_j];
    pk = new real[ext_size_i * ext_size_j];
    pkp1 = new real[ext_size_i * ext_size_j];
    r = new real[ext_size_i * ext_size_j];
    g = new real[ext_size_i * ext_size_j];
    dp = new real[ext_size_i * ext_size_j];
    dr = new real[ext_size_i * ext_size_j];
    dg = new real[ext_size_i * ext_size_j];

    if (top >= 0) {
        send_t = new real[brd_size_j];
        recv_t = new real[brd_size_j];
    }

    if (right >= 0) {
        send_r = new real[brd_size_i];
        recv_r = new real[brd_size_i];
    }

    if (bottom >= 0) {
        send_b = new real[brd_size_j];
        recv_b = new real[brd_size_j];
    }

    if (left >= 0) {
        send_l = new real[brd_size_i];
        recv_l = new real[brd_size_i];
    }

    reqs_send = new MPI_Request[4];
    reqs_recv = new MPI_Request[4];
}

void Poisson::destroy()
{
    delete []F;
    delete []pk;
    delete []pkp1;
    delete []r;
    delete []g;
    delete []dp;
    delete []dr;
    delete []dg;

    if (top >= 0) {
        delete []send_t;
        delete []recv_t;
    }

    if (right >= 0) {
        delete []send_r;
        delete []recv_r;
    }

    if (bottom >= 0) {
        delete []send_b;
        delete []recv_b;
    }

    if (left >= 0) {
        delete []send_l;
        delete []recv_l;
    }

    delete []reqs_send;
    delete []reqs_recv;
}

void pair_add(void *in_, void *inout_, int *len, MPI_Datatype *dptr)
{
    mpi_pair *in = (mpi_pair *)in_;
    mpi_pair *inout = (mpi_pair *)inout_;
    for (int i = 0; i < *len; ++i) {
        inout[i].beta += in[i].beta;
        inout[i].gamma += in[i].gamma;
    }
}

void Poisson::init_mpi_pair()
{
    MPI_Type_contiguous(2, MPI_R, &mpi_pair_type);
    MPI_Type_commit(&mpi_pair_type);
    MPI_Op_create(pair_add, true, &merge_op);
}

void Poisson::init_F()
{
    #pragma omp parallel for
    for (int i = 0; i < ext_size_i; ++i) {
        for (int j = 0; j < ext_size_j; ++j) {
            real x = (brd_start_j + j - 1) * grid_step;
            real y = (brd_start_i + i - 1) * grid_step;
            real xpy2 = (x + y) * (x + y);
            F[i * ext_size_j + j] = (4 - 8 * xpy2) * exp(1 - xpy2);
        }
    }
}

void Poisson::init_p()
{
    int first_i = top < 0 ? 1 : 0;
    int last_i = bottom < 0 ? brd_size_i : ext_size_i - 1;
    int first_j = left < 0 ? 1 : 0;
    int last_j = right < 0 ? brd_size_j : ext_size_j - 1;
    real x, y;

    #pragma omp parallel
    {
        #pragma omp for
        for (int i = first_i; i <= last_i; ++i) {
            memset(&pk[i * ext_size_j + first_j], 0,
                (last_j - first_j + 1) * sizeof(real));
        }

        if (top < 0) {
            #pragma omp for private(x)
            for (int j = 0; j < ext_size_j; ++j) {
                x = (brd_start_j + j - 1) * grid_step;  // y == 0
                pk[j] = exp(1 - x * x);
                pkp1[j] = exp(1 - x * x);
            }
        }

        if (right < 0) {
            #pragma omp for private(y)
            for (int i = 0; i < ext_size_i; ++i) {
                y = (brd_start_i + i - 1) * grid_step;  // x == 2
                pk[(i + 1) * ext_size_j - 1] = exp(1 - (2 + y) * (2 + y));
                pkp1[(i + 1) * ext_size_j - 1] = exp(1 - (2 + y) * (2 + y));
            }
        }

        if (bottom < 0) {
            #pragma omp for private(x)
            for (int j = 0; j < ext_size_j; ++j) {
                x = (brd_start_j + j - 1) * grid_step;  // y == 2
                pk[(ext_size_i - 1) * ext_size_j + j] =
                    exp(1 - (x + 2) * (x + 2));
                pkp1[(ext_size_i - 1) * ext_size_j + j] =
                    exp(1 - (x + 2) * (x + 2));
            }
        }

        if (left < 0) {
            #pragma omp for private(y)
            for (int i = 0; i < ext_size_i; ++i) {
                y = (brd_start_i + i - 1) * grid_step;  // x == 0
                pk[i * ext_size_j] = exp(1 - y * y);
                pkp1[i * ext_size_j] = exp(1 - y * y);
            }
        }
    }
}

void Poisson::init()
{
    init_mpi_size_xy();
    init_neighbors();
    init_brd_size_ij();
    init_mem();
    init_mpi_pair();

    init_F();
    init_p();
    init_ext(r);
    init_ext(g);
    init_ext(dp);
    init_ext(dr);
    init_ext(dg);
}

void Poisson::laplacian(real *dst, const real *src, int i, int j)
{
    dst[i * ext_size_j + j] = (
        (src[i * ext_size_j + j] - src[(i - 1) * ext_size_j + j]) -
        (src[(i + 1) * ext_size_j + j] - src[i * ext_size_j + j]) +
        (src[i * ext_size_j + j] - src[i * ext_size_j + j - 1]) -
        (src[i * ext_size_j + j + 1] - src[i * ext_size_j + j])
    ) / sqr_grid_step;
}

void Poisson::laplacian_brd(real *dst, const real *src)
{
    #pragma omp parallel
    {
        #pragma omp for
        for (int j = 1; j <= brd_size_j; ++j)
            laplacian(dst, src, 1, j);

        #pragma omp for
        for (int i = 1; i <= brd_size_i; ++i)
            laplacian(dst, src, i, brd_size_j);

        #pragma omp for
        for (int j = 1; j <= brd_size_j; ++j)
            laplacian(dst, src, brd_size_i, j);

        #pragma omp for
        for (int i = 1; i <= brd_size_i; ++i)
            laplacian(dst, src, i, 1);
    }
}

void Poisson::laplacian_innbrd(real *dst, const real *src, bool brd)
{
    int first_i = brd ? 1 : 2;
    int last_i = brd ? brd_size_i : brd_size_i - 1;
    int first_j = first_i;
    int last_j = brd ? brd_size_j : brd_size_j - 1;

    #pragma omp parallel for
    for (int i = first_i; i <= last_i; ++i) {
        for (int j = first_j; j <= last_j; ++j) {
            laplacian(dst, src, i, j);
        }
    }
}

void Poisson::update_r_ext()
{
    #pragma omp parallel
    {
        if (top >= 0) {
            #pragma omp for
            for (int j = 0; j < ext_size_j; ++j)
                r[j] = dp[j] - F[j];
        }

        if (right >= 0) {
            #pragma omp for
            for (int i = 0; i < ext_size_i; ++i)
                r[(i + 1) * ext_size_j - 1] = dp[(i + 1) * ext_size_j - 1] -
                    F[(i + 1) * ext_size_j - 1];
        }

        if (bottom >= 0) {
            #pragma omp for
            for (int j = 0; j < ext_size_j; ++j)
                r[(ext_size_i - 1) * ext_size_j + j] =
                    dp[(ext_size_i - 1) * ext_size_j + j] -
                    F[(ext_size_i - 1) * ext_size_j + j];
        }

        if (left >= 0) {
            #pragma omp for
            for (int i = 0; i < ext_size_i; ++i)
                r[i * ext_size_j] = dp[i * ext_size_j] - F[i * ext_size_j];
        }
    }
}

void Poisson::update_r_innbrd()
{
    #pragma omp parallel for
    for (int i = 1; i <= brd_size_i; ++i) {
        for (int j = 1; j <= brd_size_j; ++j) {
            r[i * ext_size_j + j] = dp[i * ext_size_j + j] -
                F[i * ext_size_j + j];
        }
    }
}

void Poisson::update_g()
{
    int first_i = top < 0 ? 1 : 0;
    int last_i = bottom < 0 ? brd_size_i : ext_size_i - 1;
    int first_j = left < 0 ? 1 : 0;
    int last_j = right < 0 ? brd_size_j : ext_size_j - 1;

    #pragma omp parallel for
    for (int i = first_i; i <= last_i; ++i) {
        for (int j = first_j; j <= last_j; ++j) {
            g[i * ext_size_j + j] = r[i * ext_size_j + j] -
                alpha_global * g[i * ext_size_j + j];
        }
    }
}

void Poisson::update_p()
{
    int first_i = top < 0 ? 1 : 0;
    int last_i = bottom < 0 ? brd_size_i : ext_size_i - 1;
    int first_j = left < 0 ? 1 : 0;
    int last_j = right < 0 ? brd_size_j : ext_size_j - 1;

    #pragma omp parallel for
    for (int i = first_i; i <= last_i; ++i) {
        for (int j = first_j; j <= last_j; ++j) {
            pkp1[i * ext_size_j + j] = pk[i * ext_size_j + j] -
                tau * g[i * ext_size_j + j];
        }
    }
}

real Poisson::dot_brd(const real *a, const real *b)
{
    real result = 0.0;

    #pragma omp parallel reduction(+:result)
    {
        #pragma omp for
        for (int j = 1; j < brd_size_j; ++j)  // i == 1
            result += sqr_grid_step * a[ext_size_j + j] * b[ext_size_j + j];

        #pragma omp for
        for (int i = 1; i < brd_size_i; ++i)  // j == brd_size_j
            result += sqr_grid_step * a[(i + 1) * ext_size_j - 2] *
                b[(i + 1) * ext_size_j - 2];

        #pragma omp for
        for (int j = 2; j <= brd_size_j; ++j)  // i == brd_size_i
            result += sqr_grid_step * a[brd_size_i * ext_size_j + j] *
                b[brd_size_i * ext_size_j + j];

        #pragma omp for
        for (int i = 2; i <= brd_size_i; ++i)  // j == 1
            result += sqr_grid_step * a[i * ext_size_j + 1] *
                b[i * ext_size_j + 1];
    }

    return result;
}

real Poisson::dot_innbrd(const real *a, const real *b, bool brd)
{
    int first_i = brd ? 1 : 2;
    int last_i = brd ? brd_size_i : brd_size_i - 1;
    int first_j = first_i;
    int last_j = brd ? brd_size_j : brd_size_j - 1;

    real result = 0.0;

    #pragma omp parallel for reduction(+:result)
    for (int i = first_i; i <= last_i; ++i) {
        for (int j = first_j; j <= last_j; ++j) {
            result += sqr_grid_step * a[i * ext_size_j + j] *
                b[i * ext_size_j + j];
        }
    }

    return result;
}

real Poisson::criterion()
{
    real local = 0.0, global = 0.0;

    #pragma omp parallel for reduction(+:local)
    for (int i = 1; i <= brd_size_i; ++i) {
        for (int j = 1; j <= brd_size_j; ++j) {
            real diff = pkp1[i * ext_size_j + j] - pk[i * ext_size_j + j];
            local += sqr_grid_step * diff * diff;
        }
    }

    MPI_Allreduce(&local, &global, 1, MPI_R, MPI_SUM, MPI_COMM_WORLD);

    return sqrt(global);
}

void Poisson::merge_alpha()
{
    alpha_global = 0.0;
    MPI_Allreduce(&alpha_local, &alpha_global, 1, MPI_R, MPI_SUM,
        MPI_COMM_WORLD);
    alpha_global /= dgg;
}

void Poisson::merge_beta_gamma()
{
    mpi_pair result;
    result.beta = result.gamma = 0.0;

    MPI_Allreduce(&beta_gamma, &result, 1, mpi_pair_type, merge_op,
        MPI_COMM_WORLD);

    dgg = result.gamma;
    tau = result.beta / result.gamma;
}

void Poisson::to_buffers()
{
    #pragma omp parallel
    {
        if (top >= 0) {
            #pragma omp for
            for (int j = 1; j <= brd_size_j; ++j)  // i == 1
                send_t[j - 1] = dp[ext_size_j + j];
        }

        if (right >= 0) {
            #pragma omp for
            for (int i = 1; i <= brd_size_i; ++i)  // j == brd_size_j
                send_r[i - 1] = dp[(i + 1) * ext_size_j - 2];
        }

        if (bottom >= 0) {
            #pragma omp for
            for (int j = 1; j <= brd_size_j; ++j)  // i == brd_size_i
                send_b[j - 1] = dp[brd_size_i * ext_size_j + j];
        }

        if (left >= 0) {
            #pragma omp for
            for (int i = 1; i <= brd_size_i; ++i)  // j == 1
                send_l[i - 1] = dp[i * ext_size_j + 1];
        }
    }
}

void Poisson::from_buffers()
{
    #pragma omp parallel
    {
        if (top >= 0) {
            #pragma omp for
            for (int j = 1; j <= brd_size_j; ++j)  // i == 0
                dp[j] = recv_t[j - 1];
        }

        if (right >= 0) {
            #pragma omp for
            for (int i = 1; i <= brd_size_i; ++i)  // j == ext_size_j - 1
                dp[(i + 1) * ext_size_j - 1] = recv_r[i - 1];
        }

        if (bottom >= 0) {
            #pragma omp for
            for (int j = 1; j <= brd_size_j; ++j)  // i == ext_size_i - 1
                dp[(ext_size_i - 1) * ext_size_j + j] = recv_b[j - 1];
        }

        if (left >= 0) {
            #pragma omp for
            for (int i = 1; i <= brd_size_i; ++i)  // j == 0
                dp[i * ext_size_j] = recv_l[i - 1];
        }
    }
}

void Poisson::exchange()
{
    reqs_count = 0;

    if (top >= 0) {
        MPI_Isend(send_t, brd_size_j, MPI_R, top, 0, MPI_COMM_WORLD,
            &reqs_send[reqs_count]);
        MPI_Irecv(recv_t, brd_size_j, MPI_R, top, 0, MPI_COMM_WORLD,
            &reqs_recv[reqs_count++]);
    }

    if (right >= 0) {
        MPI_Isend(send_r, brd_size_i, MPI_R, right, 0, MPI_COMM_WORLD,
            &reqs_send[reqs_count]);
        MPI_Irecv(recv_r, brd_size_i, MPI_R, right, 0, MPI_COMM_WORLD,
            &reqs_recv[reqs_count++]);
    }

    if (bottom >= 0) {
        MPI_Isend(send_b, brd_size_j, MPI_R, bottom, 0, MPI_COMM_WORLD,
            &reqs_send[reqs_count]);
        MPI_Irecv(recv_b, brd_size_j, MPI_R, bottom, 0, MPI_COMM_WORLD,
            &reqs_recv[reqs_count++]);
    }

    if (left >= 0) {
        MPI_Isend(send_l, brd_size_i, MPI_R, left, 0, MPI_COMM_WORLD,
            &reqs_send[reqs_count]);
        MPI_Irecv(recv_l, brd_size_i, MPI_R, left, 0, MPI_COMM_WORLD,
            &reqs_recv[reqs_count++]);
    }
}

void Poisson::wait()
{
    MPI_Waitall(reqs_count, reqs_send, MPI_STATUS_IGNORE);
    MPI_Waitall(reqs_count, reqs_recv, MPI_STATUS_IGNORE);
}

void swap(real **a, real **b)
{
    real *tmp = *a;
    *a = *b;
    *b = tmp;
}

void Poisson::loop()
{
    int iteration = 0;
    double total_time = 0.0;
    double wait_time = 0.0;
    struct timeval t0, t1, t2, t3;

    while (true)
    {
        ++iteration;

        gettimeofday(&t0, 0);
        laplacian_brd(dp, pk);
        to_buffers();
        exchange();
        laplacian_innbrd(dp, pk, false);
        update_r_innbrd();

        if (iteration > 1) {
            laplacian_innbrd(dr, r, false);
            alpha_local = dot_innbrd(dr, g, false);
        }

        gettimeofday(&t1, 0);
        total_time += t1.tv_sec - t0.tv_sec +
            1e-6 * (t1.tv_usec - t0.tv_usec);

        wait();

        gettimeofday(&t2, 0);
        total_time += t2.tv_sec - t1.tv_sec +
            1e-6 * (t2.tv_usec - t1.tv_usec);
        wait_time += t2.tv_sec - t1.tv_sec +
            1e-6 * (t2.tv_usec - t1.tv_usec);

        from_buffers();
        update_r_ext();

        if (iteration > 1) {
            laplacian_brd(dr, r);
            alpha_local += dot_brd(dr, g);
            merge_alpha();
            update_g();
            beta_gamma.beta = dot_innbrd(r, g, true);
        } else {
            swap(&r, &g);
            beta_gamma.beta = dot_innbrd(g, g, true);
        }

        laplacian_innbrd(dg, g, true);
        beta_gamma.gamma = dot_innbrd(dg, g, true);
        merge_beta_gamma();
        update_p();
        real crit = criterion();

        gettimeofday(&t3, 0);
        total_time += t3.tv_sec - t2.tv_sec +
            1e-6 * (t3.tv_usec - t2.tv_usec);

        if (crit < eps)
            break;

        swap(&pk, &pkp1);
    }

    if (mpi_rank == 0) {
        std::cout << iteration << " iterations completed in " <<
            total_time << " s" << std::endl;
        std::cout << wait_time << " s wasted" << std::endl;
    }
}

void Poisson::save(const char *path)
{
    int first_i = top < 0 ? 0 : 1;
    int last_i = bottom < 0 ? ext_size_i - 1 : brd_size_i;
    int first_j = left < 0 ? 0 : 1;
    int last_j = right < 0 ? ext_size_j - 1 : brd_size_j;
    MPI_Status status;

    if (mpi_rank == 0) {
        std::ofstream f(path, std::ios::out | std::ios::binary);

        for (int i = first_i; i <= last_i; ++i) {
            for (int j = first_j; j <= last_j; ++j) {
                int global_i = brd_start_i + i - 1;
                int global_j = brd_start_j + j - 1;

                f.write((char *)&global_i, sizeof(int));
                f.write((char *)&global_j, sizeof(int));
                f.write((char *)&pkp1[i * ext_size_j + j], sizeof(real));
            }
        }

        for (int r = 1; r < mpi_size; ++r) {
            MPI_Recv(&ext_size_i, 1, MPI_INT, r, 1, MPI_COMM_WORLD, &status);
            MPI_Recv(&ext_size_j, 1, MPI_INT, r, 2, MPI_COMM_WORLD, &status);
            MPI_Recv(&brd_start_i, 1, MPI_INT, r, 3, MPI_COMM_WORLD, &status);
            MPI_Recv(&brd_start_j, 1, MPI_INT, r, 4, MPI_COMM_WORLD, &status);

            MPI_Recv(pkp1, ext_size_i * ext_size_j, MPI_R, r, 0,
                MPI_COMM_WORLD, &status);

            first_i = (r / mpi_size_x == 0) ? 0 : 1;
            last_i = (r / mpi_size_x == mpi_size_y - 1) ? ext_size_i - 1 :
                ext_size_i - 2;
            first_j = (r % mpi_size_x == 0) ? 0 : 1;
            last_j = (r % mpi_size_x == mpi_size_x - 1) ? ext_size_j - 1 :
                ext_size_j - 2;

            for (int i = first_i; i <= last_i; ++i) {
                for (int j = first_j; j <= last_j; ++j) {
                    int global_i = brd_start_i + i - 1;
                    int global_j = brd_start_j + j - 1;

                    f.write((char *)&global_i, sizeof(int));
                    f.write((char *)&global_j, sizeof(int));
                    f.write((char *)&pkp1[i * ext_size_j + j], sizeof(real));
                }
            }
        }

        f.close();
    } else {
        MPI_Send(&ext_size_i, 1, MPI_INT, 0, 1, MPI_COMM_WORLD);
        MPI_Send(&ext_size_j, 1, MPI_INT, 0, 2, MPI_COMM_WORLD);
        MPI_Send(&brd_start_i, 1, MPI_INT, 0, 3, MPI_COMM_WORLD);
        MPI_Send(&brd_start_j, 1, MPI_INT, 0, 4, MPI_COMM_WORLD);

        MPI_Send(pkp1, ext_size_i * ext_size_j, MPI_R, 0, 0,
            MPI_COMM_WORLD);
    }
}

