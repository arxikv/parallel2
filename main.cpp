#include <cstdlib>

#include "poisson.h"

int main(int argc, char **argv)
{
    int mpi_size;
    int mpi_rank;
    int grid_size = atoi(argv[1]);
    double eps = 1e-4;

    if (argc == 4)
        eps = atof(argv[3]);

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);

    Poisson poisson(mpi_size, mpi_rank, grid_size, eps);
    poisson.init();
    poisson.loop();
    poisson.save(argv[2]);
    poisson.destroy();

    MPI_Finalize();

    return 0;
}

